# Gulp, Webpack and Babel boilerplate - ES6 #

__note - depending on your installed node version, you may need to rebuild libsass__

### usage ###

$ npm install

Check that within the dist/ folder, there is a build folder and css folder within that. Your file setup should be...

```
dist    
│
└───build
    │
    ├───css
  
Then run...

$ gulp

Go to http://localhost:8080/webpack-dev-server/